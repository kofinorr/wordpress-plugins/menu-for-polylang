<?php

use KrrMenuForPolylang\Plugin;

$item    = $item ?? null;
$item_id = $item_id ?? null;

/* Get the default value */
$itemUrl = $item->url;
?>

<p class="description description-wide krr-mfpll-fields">
	<label>
		<?php _e('URL custom', 'krr-mfpll'); ?><br/>

		<?php foreach (pll_languages_list() as $locale) {

			/* Get the custom value */
			$customItemUrl = Plugin::getFieldValue($item_id, 'url', $locale);

			/* Edit the final url if there is a custom value */
			if ($customItemUrl) {
				$itemUrl = $customItemUrl;
			}

			?>
			<span class="krr-mfpll-field">
				<span class="krr-mfpll-locale"><?= $locale ?></span>
				<input type="text"
					   id="<?= Plugin::getBaseFieldName('url', $locale) ?>-<?= $item_id ?>"
					   class="widefat edit-menu-item-locale-url"
					   name="<?= Plugin::getBaseFieldName('url', $locale) ?>[<?= $item_id ?>]"
					   value="<?= esc_attr($itemUrl) ?>"/>
			</span>
		<?php } ?>
	</label>
</p>