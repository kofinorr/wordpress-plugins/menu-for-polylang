<?php
/**
 * Plugin Name: Menu For Polylang
 * Description: Simplify the translation of menus with Polylang.
 * Version: 1.0.0
 * Author: Kalimorr
 * Author URI: https://gitlab.com/kalimorr/
 * Text Domain: krr-mfpll
 * Domain Path: /languages
 * License: GPLv3 or later
 */

/* Security */
defined('ABSPATH')
or die ('no');

define('KRR_MFPLL_FILE', __FILE__);

/* Call the helpers  */
require_once 'helper/StringFunctions.php';

/* Call the plugin files */
require_once 'classes/Dependencies.php';
require_once 'classes/AdminMenu.php';
require_once 'classes/MenuRender.php';
require_once 'classes/Plugin.php';

new KrrMenuForPolylang\Plugin();