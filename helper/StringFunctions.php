<?php

namespace KrrMenuForPolylang\Helper;

/**
 * Class StringFunctions
 *
 * @package KrrMenuForPolylang\Helper
 */
class StringFunctions
{
	/**
	 * Check if the string ends with the needle
	 *
	 * @param $haystack
	 * @param $needle
	 *
	 * @return bool
	 */
	public static function endsWith($haystack, $needle)
	{
		$length = strlen($needle);

		if (!$length) {
			return true;
		}

		return substr($haystack, -$length) === $needle;
	}
}