<?php

use KrrMenuForPolylang\Plugin;

$allowTranslations = $allowTranslations ?? null;
$item              = $item ?? null;
$item_id           = $item_id ?? null;

/* Get the default value */
$itemTitle = $item->title;
?>

<p class="description description-wide krr-mfpll-fields">
	<label>
		<?php _e('Navigation Label', 'krr-mfpll'); ?><br/>

		<?php foreach (pll_languages_list() as $locale) {

			/* Get the custom value */
			$customItemTitle = Plugin::getFieldValue($item_id, 'title', $locale);

			/* Edit the final title if there is a custom value */
			if ($customItemTitle) {
				$itemTitle = $customItemTitle;
			} ?>

			<span class="krr-mfpll-field">
				<span class="krr-mfpll-locale"><?= $locale ?></span>
				<input type="text"
					   id="<?= Plugin::getBaseFieldName('title', $locale) ?>-<?= $item_id ?>"
					   class="widefat edit-menu-item-locale-title"
					   name="<?= Plugin::getBaseFieldName('title', $locale) ?>[<?= $item_id ?>]"
					   value="<?= esc_attr($itemTitle) ?>"/>
			</span>
		<?php } ?>

		<?php if (!$allowTranslations) { ?>
			<span class="krr-mfpll-translation-warning"><?php _e('This post type does not support translations. Please check your Polylang configuration if this is a mistake.') ?></span>
		<?php } ?>

	</label>
</p>