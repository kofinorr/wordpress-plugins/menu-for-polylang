<?php

namespace KrrMenuForPolylang;

use KrrMenuForPolylang\Helper\StringFunctions;

/**
 * Class AdminMenu
 *
 * @package KrrMenuForPolylang
 */
class AdminMenu
{
	const CONFIG_METAKEY = 'krr-menu-for-polylang-config';

	/**
	 * AdminMenu constructor.
	 */
	public function __construct()
	{
		add_action('wp_update_nav_menu', [$this, 'updateConfig'], 50);
		add_action('wp_nav_menu_item_custom_fields', [$this, 'create'], 10, 5);
		add_action('wp_update_nav_menu_item', [$this, 'update'], 10, 3);
		add_filter('wp_setup_nav_menu_item', [$this, 'addLocaleData']);
	}

	/**
	 * Check if current menu in admin page is translatable
	 *
	 * @return bool
	 */
	public static function isCurrentTranslate()
	{
		$currentMenu = $_GET['menu'] ?? wp_get_nav_menus()[0]->term_id;

		return Plugin::isTranslatable($currentMenu);
	}

	/**
	 * Create the menu fields
	 *
	 * @param $item_id
	 * @param $item
	 * @param $depth
	 * @param $args
	 * @param $id
	 */
	public function create($item_id, $item, $depth, $args, $id)
	{
		if (!self::isCurrentTranslate()) {
			return;
		}

		/* Check if the post type configuration allow translation */
		$allowTranslations = ($item->type !== 'post_type' || in_array(
				'post_translations',
				get_object_taxonomies($item->object))
		);

		include plugin_dir_path(KRR_MFPLL_FILE) . 'views/adminMenuFields/label.php';

		if ($item->object === 'custom') {
			include plugin_dir_path(KRR_MFPLL_FILE) . 'views/adminMenuFields/url.php';
		}
	}


	/**
	 * Save new field navmenu
	 *
	 * @param $menu_id
	 * @param $menu_item_db_id
	 * @param $args
	 */
	public function update($menu_id, $menu_item_db_id, $args)
	{
		foreach (pll_languages_list() as $locale) {
			$this->updateFieldValue($menu_item_db_id, 'title', $locale, $args);

			if ($args['menu-item-type'] === 'custom') {
				$this->updateFieldValue($menu_item_db_id, 'url', $locale);
			}
		}
	}

	/**
	 * Update post meta on new field navmenu saving
	 *
	 * @param        $menu_item_db_id
	 * @param string $type
	 * @param string $locale
	 * @param array  $args
	 */
	private function updateFieldValue($menu_item_db_id, string $type, string $locale, $args = [])
	{
		$slug = Plugin::getBaseFieldName($type, $locale);

		if (isset($_REQUEST[$slug]) && is_array($_REQUEST[$slug])) {
			/* Get the new value from the http request */
			$custom_value = $_REQUEST[$slug][$menu_item_db_id];

			/* Update the post meta of the menu item */
			update_post_meta($menu_item_db_id, $slug, $custom_value);
		}
	}

	/**
	 * Adds value of new field to nav menu object
	 *
	 * @param $menu_item
	 *
	 * @return mixed
	 */
	public function addLocaleData($menu_item)
	{
		$menu_item->KRR_MFPLL = [
			'title' => []
		];

		/* Add the url entry for custom type item */
		if ($menu_item->object === 'custom') {
			$menu_item->KRR_MFPLL['url'] = [];
		}

		/* Object for each language the correct value */
		foreach (pll_languages_list() as $locale) {
			/* Get title field value */
			$title = Plugin::getFieldValue($menu_item->ID, 'title', $locale);

			/* Set title field value for this item */
			$menu_item->KRR_MFPLL['title'][$locale] = $title;

			if ($menu_item->object === 'custom') {
				$menu_item->KRR_MFPLL['url'][$locale] = Plugin::getFieldValue($menu_item->ID, 'url', $locale);
			}
		}

		return $menu_item;
	}

	/**
	 * Update configuration of menus translations
	 */
	public function updateConfig()
	{
		/* Get languages data */
		$locale    = pll_default_language();
		$languages = pll_languages_list();

		/* Get the registered menu locations */
		$locations  = get_nav_menu_locations();
		$translated = [];

		foreach ($locations as $location => $menuID) {
			$menu = wp_get_nav_menu_object($menuID);

			if (!$menu){
				continue;
			}

			foreach ($languages as $language) {
				/* Don't test the default language, neither the default menu location (for the default language) */
				if ($language === $locale || !StringFunctions::endsWith($location, '___' . $language)) {
					continue;
				}

				/* Get the menu location without the suffix of the language. Thereby, "primarry___en" becomes "primary" */
				$locationSlug = preg_replace('/___' . $language . '$/', '', $location);

				/* Add the menu in the list only if it is the same for the default language and if it is not already in the list */
				if ($menuID === $locations[$locationSlug] && !in_array($menu->slug, $translated)) {
					array_push($translated, $menu->slug);
				}
			}
		}

		/* Update the in database the list of translated menus */
		update_option(self::CONFIG_METAKEY, $translated);
	}
}